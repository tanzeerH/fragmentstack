package com.fragmentstack;

import java.util.Stack;

import com.fragmentstack.fragment.FragmentOne;
import com.fragmentstack.fragment.FragmentThree;
import com.fragmentstack.fragment.FragmentTwo;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;
import android.os.Build;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements OnTabChangeListener {

	ActionBar actionBar;
	TabHost tabHost;
	public Stack<Fragment> tabStack1 = new Stack<Fragment>();
	public Stack<Fragment> tabStack2 = new Stack<Fragment>();
	public Stack<Fragment> tabStack3 = new Stack<Fragment>();
	private int curSelection = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup();
		TabSpec tabspec1 = tabHost.newTabSpec("tab1");
		tabspec1.setIndicator("t1");
		tabspec1.setContent(new TabFactory(this));

		TabSpec tabspec2 = tabHost.newTabSpec("tab2");
		tabspec2.setIndicator("t2");
		tabspec2.setContent(new TabFactory(this));

		TabSpec tabspec3 = tabHost.newTabSpec("tab3");
		tabspec3.setIndicator("t3");
		tabspec3.setContent(new TabFactory(this));

		tabHost.addTab(tabspec1);
		tabHost.addTab(tabspec2);
		tabHost.addTab(tabspec3);

		// this.onTabChanged("tab1");
		setInitialTab();
		tabHost.setOnTabChangedListener(this);

	}

	private void setInitialTab() {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		FragmentOne fragmentOne = new FragmentOne();
		ft.replace(android.R.id.tabcontent, fragmentOne);
		ft.commit();
	}

	class TabFactory implements TabContentFactory {

		private final Context mContext;

		/**
		 * @param context
		 */
		public TabFactory(Context context) {
			mContext = context;
		}

		/**
		 * (non-Javadoc)
		 * 
		 * @see android.widget.TabHost.TabContentFactory#createTabContent(java.lang.String)
		 */
		public View createTabContent(String tag) {
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabChanged(String tabId) {

		Toast.makeText(getApplicationContext(), tabId, Toast.LENGTH_SHORT).show();
		if (tabId == "tab1") {
			{
				hidePreviousTabFragment();
				curSelection = 1;
				if (tabStack1.size() == 0) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					FragmentOne fragmentOne = new FragmentOne();
					ft.add(android.R.id.tabcontent, fragmentOne);
					ft.commit();
				} else {

					getTopOfBackStack();
				}
			}

		} else if (tabId == "tab2") {
			{
				hidePreviousTabFragment();
				curSelection = 2;
				if (tabStack2.size() == 0) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					FragmentTwo fragmentTwo = new FragmentTwo();
					ft.add(android.R.id.tabcontent, fragmentTwo);
					ft.commit();
				} else {
					getTopOfBackStack();

				}
			}

		}
		else if (tabId == "tab3") {
			{
				hidePreviousTabFragment();
				curSelection = 3;
				if (tabStack3.size() == 0) {
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					FragmentThree fragmentThree = new FragmentThree();
					ft.add(android.R.id.tabcontent, fragmentThree);
					ft.commit();
				} else {
					getTopOfBackStack();

				}
			}

		}
		

	}

	private void getTopOfBackStack() {
		if (curSelection == 1) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			int size = tabStack1.size();
			Fragment fragment = tabStack1.get(size - 1);
			ft.show(fragment);
			ft.commit();
		} else if (curSelection == 2) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			int size = tabStack2.size();
			Fragment fragment = tabStack2.get(size - 1);
			ft.show(fragment);
			ft.commit();
		}
		else if (curSelection == 3) {
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			int size = tabStack3.size();
			Fragment fragment = tabStack3.get(size - 1);
			ft.show(fragment);
			ft.commit();
		}
	}

	private void hidePreviousTabFragment() {
		if (curSelection == 1) {
			int size = tabStack1.size();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment fragment = tabStack1.get(size - 1);
			ft.hide(fragment);
			ft.commit();
		} else if (curSelection == 2) {
			int size = tabStack2.size();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment fragment = tabStack2.get(size - 1);
			ft.hide(fragment);
			ft.commit();

		} else if (curSelection == 3) {
			int size = tabStack3.size();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			Fragment fragment = tabStack3.get(size - 1);
			ft.hide(fragment);
			ft.commit();


		}
	}

	@Override
	public void onBackPressed() {

		// super.onBackPressed();
		manageBackPressed();

	}

	private void manageBackPressed() {
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		// FragmentOne fragmentOne=new FragmentOne();
		if (curSelection == 1) {
			if (tabStack1.size() > 1) {
				Fragment poppedFragment = tabStack1.pop();
				int size = tabStack1.size();
				ft.hide(poppedFragment);
				Log.e("msg", "" + size);
				Fragment fragment = tabStack1.get(size - 1);
				// fragment=tabStack1.get(location)
				// ft.replace(android.R.id.tabcontent, fragment);
				ft.show(fragment);
				ft.commit();
			} else {
				finish();
			}
		} else if (curSelection == 2) {
			if (tabStack2.size() > 1) {
				Fragment poppedFragment = tabStack2.pop();
				ft.hide(poppedFragment);
				int size = tabStack2.size();
				Log.e("msg", "" + size);
				Fragment fragment = tabStack2.get(size - 1);
				// fragment=tabStack1.get(location)
				// ft.replace(android.R.id.tabcontent, fragment);
				ft.show(fragment);
				ft.commit();
			} else {
				finish();
			}
		}
		else if (curSelection == 3) {
			if (tabStack3.size() > 1) {
				Fragment poppedFragment = tabStack3.pop();
				ft.hide(poppedFragment);
				int size = tabStack3.size();
				Log.e("msg", "" + size);
				Fragment fragment = tabStack3.get(size - 1);
				// fragment=tabStack1.get(location)
				// ft.replace(android.R.id.tabcontent, fragment);
				ft.show(fragment);
				ft.commit();
			} else {
				finish();
			}
		}

	}

}
