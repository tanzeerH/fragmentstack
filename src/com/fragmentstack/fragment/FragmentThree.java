package com.fragmentstack.fragment;

import com.fragmentstack.MainActivity;
import com.fragmentstack.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Choreographer.FrameCallback;

public class FragmentThree  extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_three, null,false);
		((MainActivity)getActivity()).tabStack3.push(FragmentThree.this);
		return v;
	}

}
