package com.fragmentstack.fragment;

import com.fragmentstack.MainActivity;
import com.fragmentstack.R;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FragmentTwoChildOne extends Fragment {
	String name = "Fragment Two Child One";

	Button btn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_two_child_one, null, false);
		btn = (Button) v.findViewById(R.id.button1);
		Log.e(name, "OncreateView");
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
				FragmentTwoChildtwo fragmentTwoChildtwo = new FragmentTwoChildtwo();
				ft.hide(FragmentTwoChildOne.this);
				// ft.addToBackStack(null);
				ft.add(android.R.id.tabcontent, fragmentTwoChildtwo);
				ft.commit();

			}
		});
		((MainActivity) getActivity()).tabStack2.push(FragmentTwoChildOne.this);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.e(name, "On Activity Created");
	}

	@Override
	public void onStart() {
		super.onStart();
		Log.e(name, "Onstart");
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e(name, "OnResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.e(name, "OnPause");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.e(name, "Onstop");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Log.e(name, "OnDestryView");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e(name, "OnDestroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
		Log.e(name, "Ondetach");
	}

}
