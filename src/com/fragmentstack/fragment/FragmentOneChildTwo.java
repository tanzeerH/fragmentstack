package com.fragmentstack.fragment;

import com.fragmentstack.MainActivity;
import com.fragmentstack.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Choreographer.FrameCallback;

public class FragmentOneChildTwo  extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=inflater.inflate(R.layout.fragment_one_child_two, null,false);
		
		((MainActivity)getActivity()).tabStack1.push(FragmentOneChildTwo.this);
		return v;
	}

}
